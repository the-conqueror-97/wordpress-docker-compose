# WordPress Docker Setup

This repository contains a Docker Compose setup for running WordPress with a MySQL database. The setup allows you to easily deploy a WordPress instance and provides a way to see and edit the WordPress files from your host machine.

## Prerequisites

Before you begin, ensure you have the following installed:

- [Docker](https://docs.docker.com/get-docker/)
- [Docker Compose](https://docs.docker.com/compose/install/)

## Setup

1. **Clone the repository**

    ```sh
    git clone https://github.com/yourusername/wordpress-docker-setup.git
    cd wordpress-docker-setup
    ```

2. **Create a directory for WordPress data**

    ```sh
    mkdir wordpress_data
    ```

3. **Start the containers**

    Navigate to the directory containing the `docker-compose.yml` file and run:

    ```sh
    docker-compose up -d
    ```

    This command will pull the necessary Docker images and start the containers in detached mode.

## Accessing WordPress

Once the containers are up and running, you can access your WordPress site by navigating to `http://localhost:8080` in your web browser.

## Editing WordPress Files

The WordPress files are mapped to the `wordpress_data` directory on your host machine. You can edit these files directly, and the changes will be reflected inside the WordPress container.

## Stopping the Containers

To stop the running containers, navigate to the directory containing the `docker-compose.yml` file and run:

```sh
docker-compose down
