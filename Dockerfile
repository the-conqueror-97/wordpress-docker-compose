FROM wordpress:latest

# Install WP-CLI and zip
RUN apt-get update && \
    apt-get install -y less mariadb-client zip && \
    curl -O https://raw.githubusercontent.com/wp-cli/builds/gh-pages/phar/wp-cli.phar && \
    chmod +x wp-cli.phar && \
    mv wp-cli.phar /usr/local/bin/wp